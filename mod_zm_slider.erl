-module(mod_zm_slider).
-author("Steffen Hanikel <steffen.hanikel@gmail.com>").

-mod_title("Zeitmacht Slider").
-mod_description("A foundation based slider.").
-mod_prio(900).
-mod_depends([base]).

-export([observe_admin_edit_blocks/3, manage_schema/2]).

-include_lib("zotonic.hrl").

observe_admin_edit_blocks(#admin_edit_blocks{}, Menu, Context) ->
  [
    {2, ?__("Zeitmacht", Context), [{slider, ?__("Slider", Context)}]}
    | Menu
  ].

%% @doc The datamodel for the menu routines.
manage_schema(install, _Context) ->
    #datamodel{
      predicates=
      [{slider,
        [{title, <<"Slider">>}],
        [{undefined, collection}]
      }]
    }.
