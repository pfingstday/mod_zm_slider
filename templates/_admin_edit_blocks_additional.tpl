{% javascript %}
$('#edit-blocks-wrapper').on('click', '.block-page a.select-image-collection', function(event) {
window.zBlockConnectTrigger = this;
z_event("admin-slider-select-collection", {});
event.preventDefault();
});
{% endjavascript %}

{% wire name="admin-slider-select-collection"
action={dialog_open
subject_id=id
tabs_enabled=["find", "new"]
predicate=`slider`
template="_action_dialog_connect.tpl"
title=_"Find page"
callback="window.zAdminBlockConnectDone"}
%}
