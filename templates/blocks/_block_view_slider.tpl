{% with blk.rsc_id as id %}
{% if id.exists and id.is_visible %}
<ul data-orbit data-options="{% block slider_options %}slide_number:false;navigation_arrows:false;animation:'fade';resume_on_mouseout: true;animation_speed:2000{% endblock %}">
    {% for entry in id.media %}
    <li>
        {% ifequal entry.medium.mime "image/jpeg" %}
            <img alt="{{ entry.title }}" src="/media/inline/id/{{ entry.id }}" width="{{ entry.medium.width }}" height="{{ entry.medium.height }}">
        {% else %}
            {% media entry alt=entry.title %}
        {% endifequal %}
        {% if m.rsc[entry].body|is_undefined or m.rsc[entry].body == "" %}
            {% if m.rsc[entry].summary|is_undefined or m.rsc[entry].summary == '' %}
            {% else %}
            <div class="orbit-caption">
                <h1>{{m.rsc[entry].title}}</h1>
                <p>{{m.rsc[entry].summary}}</p>
            </div>
            {% endif %}
        {% else %}
        <div class="orbit-caption">
            {{ m.rsc[entry].body }}
        </div>
        {% endif %}
    </li>
    {% endfor %}
</ul>
{% endif %}
{% endwith %}